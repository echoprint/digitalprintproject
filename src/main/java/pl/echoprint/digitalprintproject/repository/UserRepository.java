package pl.echoprint.digitalprintproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.echoprint.digitalprintproject.entity.user.User;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {

    List<User> findAll();

    Optional<User> findUserByLogin(final String login);

    Collection<User> findAllByRole(String role);

   boolean existsByLogin(String login);
}
