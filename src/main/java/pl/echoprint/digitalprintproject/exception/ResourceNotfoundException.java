package pl.echoprint.digitalprintproject.exception;

public class ResourceNotfoundException extends RuntimeException {

    private Long resourceId;

    public ResourceNotfoundException(final Long id) {
        super("resource " + id + " have not been found.");
        resourceId = id;
    }
}
