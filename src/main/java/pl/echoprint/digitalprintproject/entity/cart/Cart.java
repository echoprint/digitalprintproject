package pl.echoprint.digitalprintproject.entity.cart;

import com.google.common.collect.Lists;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.echoprint.digitalprintproject.entity.BaseEntity;
import pl.echoprint.digitalprintproject.entity.product.Product;
import pl.echoprint.digitalprintproject.entity.user.Customer;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import java.util.List;


@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Cart extends BaseEntity {

    @ManyToMany
    private List<Product> productsInCart = Lists.newArrayList();
    @OneToOne
    private Customer owner;

}
