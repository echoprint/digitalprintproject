package pl.echoprint.digitalprintproject.entity.order;

public enum OrderStatus {
    NEW, IN_PROCESS, ON_HOLD, READY, WAITING_FOR_PICKUP, DELIVERED
}
