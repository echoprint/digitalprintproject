package pl.echoprint.digitalprintproject.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.echoprint.digitalprintproject.entity.cart.Cart;
import pl.echoprint.digitalprintproject.entity.order.Order;
import pl.echoprint.digitalprintproject.entity.order.OrderStatus;
import pl.echoprint.digitalprintproject.entity.user.Customer;
import pl.echoprint.digitalprintproject.repository.CustomerRepository;
import pl.echoprint.digitalprintproject.repository.OrderRepository;

import java.time.LocalDateTime;


@Service
@RequiredArgsConstructor
public class OrderService {

    private final UserContextService userContextService;
    private final OrderRepository orderRepository;
    private final CustomerRepository customerRepository;
    private final CartService cartService;

    public Order placeOrder() {

        Cart cart = cartService.getCurrentUserCart();
        String loggedUserEmail = userContextService.getLoggedUserEmail();
        Customer customer = customerRepository.findCustomerByLogin(loggedUserEmail).get();

        Order order = new Order(cartService.calculateTotalCost(), customer.getAddress(),
                customer.getAddress(), LocalDateTime.now(), OrderStatus.NEW, customer, cart);

        orderRepository.save(order);

        cartService.clearCart();

        return order;

    }


}
