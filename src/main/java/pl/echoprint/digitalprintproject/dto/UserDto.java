package pl.echoprint.digitalprintproject.dto;

import lombok.Data;
import pl.echoprint.digitalprintproject.entity.user.Address;
import pl.echoprint.digitalprintproject.entity.user.Role;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Set;

@Data
public class UserDto {

    private Long id;

    @Email
    @NotBlank
    @Size(max = 100)
    private String login;
    @Size(min = 8, max = 100)
    @NotBlank
    private String hashedpw;
    @Size(max = 150)
    private String firstName;
    @Size(max = 150)
    private String lastName;
    private Address address;
    private Set<Role> role;
}
