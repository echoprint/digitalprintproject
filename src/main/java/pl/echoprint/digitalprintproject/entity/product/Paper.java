package pl.echoprint.digitalprintproject.entity.product;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import pl.echoprint.digitalprintproject.entity.BaseEntity;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "paper")
@Getter
@Setter
@RequiredArgsConstructor
public class Paper extends BaseEntity {

    @Enumerated
    @Column(name= "paper_code_name")
    private PaperCodeName paperCodeName;

    private Integer weight;

    @Enumerated(EnumType.STRING)
    @Column(name = "paper__finish_type", nullable = false)
    private PaperFinishType paperFinishType;

    private String format;

    @Enumerated(EnumType.STRING)
    private ProductCategory productCategory;

    @OneToMany(mappedBy = "paper")
    private Set<Product> productsOrderedWithThisPaper;

    public Paper(final PaperCodeName paperCodeName, final Integer weight, final PaperFinishType paperFinishType,
                 final String format, final ProductCategory productCategory) {
        this.paperCodeName = paperCodeName;
        this.weight = weight;
        this.paperFinishType = paperFinishType;
        this.format = format;
        this.productCategory = productCategory;
    }
}