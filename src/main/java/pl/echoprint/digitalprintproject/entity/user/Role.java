package pl.echoprint.digitalprintproject.entity.user;

import lombok.*;
import pl.echoprint.digitalprintproject.entity.BaseEntity;

import javax.persistence.*;

@Entity
@Table(name = "roles")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Role extends BaseEntity {

    private String roleName;

}
