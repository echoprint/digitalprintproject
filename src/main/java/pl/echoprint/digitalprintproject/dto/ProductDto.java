package pl.echoprint.digitalprintproject.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.echoprint.digitalprintproject.entity.product.Price;
import pl.echoprint.digitalprintproject.entity.product.ProductCategory;


@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductDto {

    private Long id;
    private ProductCategory productCategory;
    private Long paperId;
    private String colors;
    private String endProducFormat;
    boolean duplex;
    private Integer numberOfCopies;
    private Price price;

}

