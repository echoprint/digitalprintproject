package pl.echoprint.digitalprintproject.controller;


import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import pl.echoprint.digitalprintproject.dto.ProductDto;
import pl.echoprint.digitalprintproject.entity.product.Paper;
import pl.echoprint.digitalprintproject.entity.product.Price;
import pl.echoprint.digitalprintproject.entity.product.Product;
import pl.echoprint.digitalprintproject.entity.product.ProductCategory;
import pl.echoprint.digitalprintproject.service.PaperService;
import pl.echoprint.digitalprintproject.service.PricingService;
import pl.echoprint.digitalprintproject.service.ProductService;

import java.util.List;


@Controller
@RequiredArgsConstructor
@RequestMapping(path = "/products")
public class ProductController {

    private final ProductService productService;
    private final PaperService paperService;
    private final PricingService pricingService;


    @GetMapping("/add-product")
    public String addProduct(Model model,
                             @RequestParam(value = "category", required = false) ProductCategory productCategory,
                             @RequestParam(value = "paper", required = false) Paper chosenPaper,
                             @RequestParam(value = "price", required = false) Price productPrice,
                             ProductDto productDto) {
        if (productCategory != null) {
            productDto.setProductCategory(productCategory);
        }
        if (chosenPaper != null) {
            productDto.setPaperId(chosenPaper.getId());
        }
        if (productPrice != null) {
            productDto.setPrice(productPrice);
        }
        model.addAttribute("productCategoryType", ProductCategory.values());
        model.addAttribute("paperCategoryAvaible", productCategory == null ? paperService.getAll() : paperService.getPaperByProductCategory(productCategory));
        model.addAttribute("priceForCategory", productCategory == null ? pricingService.getAll() : pricingService.getPriceByCategory(productCategory));
        model.addAttribute("addNewProduct", productDto);

        return "addProduct";
    }

    @PostMapping("/add-product")
    public String addProduct(ProductDto product) {
        productService.createNewProduct(product);

        return "redirect:/products/list";
    }

    @GetMapping("/list")
    public String getProductList(Model model) {
        List<Product> products = productService.find();
        model.addAttribute("productsList", products);

        return "productsList";
    }

}


