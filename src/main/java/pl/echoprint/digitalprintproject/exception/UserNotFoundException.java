package pl.echoprint.digitalprintproject.exception;

public class UserNotFoundException extends RuntimeException {

    private Long userId;

    public UserNotFoundException(final Long id) {
        super("User id=" +id+" have not been found.");
        userId = id;
    }
}

