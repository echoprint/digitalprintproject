package pl.echoprint.digitalprintproject.dto;

import lombok.Data;
import pl.echoprint.digitalprintproject.entity.order.OrderStatus;
import pl.echoprint.digitalprintproject.entity.user.Address;
import pl.echoprint.digitalprintproject.entity.user.Customer;

import javax.validation.constraints.FutureOrPresent;
import java.time.LocalDateTime;

@Data
public class OrderDto {

    private Long id;
    private Address customerAddress;
    private Address deliveryAddress;
    @FutureOrPresent
    private LocalDateTime dateTimeCreated;
    private OrderStatus status;
    private Customer customer;
//    private List<Product> productsInCart;
}
