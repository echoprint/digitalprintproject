package pl.echoprint.digitalprintproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.echoprint.digitalprintproject.entity.product.Price;
import pl.echoprint.digitalprintproject.entity.product.ProductCategory;

import java.util.List;

@Repository
public interface PriceRepository extends JpaRepository<Price, Long> {

    List<Price> findAllByProductCategory(ProductCategory productCategory);

    boolean existsByProductCategoryAndVolume(ProductCategory category, double volume);
}
