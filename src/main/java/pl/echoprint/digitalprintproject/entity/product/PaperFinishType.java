package pl.echoprint.digitalprintproject.entity.product;

public enum PaperFinishType {
    GLOSS, MATTE, SILK
}
