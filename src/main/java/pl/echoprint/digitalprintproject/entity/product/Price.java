package pl.echoprint.digitalprintproject.entity.product;


import lombok.*;
import pl.echoprint.digitalprintproject.entity.BaseEntity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Set;


@Entity
@Table(name = "prices")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Price extends BaseEntity {

    private double volume;

    private BigDecimal netPrice;

    @Enumerated(EnumType.STRING)
    private ProductCategory productCategory;

    @OneToMany(mappedBy = "price")
    private Set<Product> productsOrderedWithThisPrice;

    public Price(final double volume, final BigDecimal netPrice, final ProductCategory productCategory) {
        this.volume = volume;
        this.netPrice = netPrice;
        this.productCategory = productCategory;
    }
}
