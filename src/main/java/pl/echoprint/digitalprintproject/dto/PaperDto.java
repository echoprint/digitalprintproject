package pl.echoprint.digitalprintproject.dto;

import lombok.Builder;
import lombok.Data;
import pl.echoprint.digitalprintproject.entity.product.PaperCodeName;
import pl.echoprint.digitalprintproject.entity.product.PaperFinishType;
import pl.echoprint.digitalprintproject.entity.product.Product;
import pl.echoprint.digitalprintproject.entity.product.ProductCategory;

import java.util.Set;

@Data
@Builder
public class PaperDto {

    private Long id;
    private PaperCodeName paperCodeName;
    private Integer weight;
    private PaperFinishType paperFinishType;
    private String format;
    private ProductCategory productCategory;
    private Set<Product> productsOrderedWithThisPaper;
}
