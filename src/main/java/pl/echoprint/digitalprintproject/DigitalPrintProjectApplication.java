package pl.echoprint.digitalprintproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DigitalPrintProjectApplication {

    public static void main(String[] args) {
        SpringApplication.run(DigitalPrintProjectApplication.class, args);
    }

}
