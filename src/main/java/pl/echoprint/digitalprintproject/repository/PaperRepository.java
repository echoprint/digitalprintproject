package pl.echoprint.digitalprintproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.echoprint.digitalprintproject.entity.product.Paper;
import pl.echoprint.digitalprintproject.entity.product.PaperCodeName;
import pl.echoprint.digitalprintproject.entity.product.ProductCategory;

import java.util.List;
import java.util.Optional;

public interface PaperRepository extends JpaRepository<Paper, Long> {

    List<Paper> findAll();

    Optional<Paper> findById(Long id);

    Optional<Paper> findByPaperCodeName(PaperCodeName paperCodeName);

    List<Paper> findAllByProductCategory(ProductCategory productCategory);

    boolean existsByProductCategoryAndAndWeight(ProductCategory productCategory, Integer weight);


}
