package pl.echoprint.digitalprintproject.component;

import lombok.RequiredArgsConstructor;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import pl.echoprint.digitalprintproject.entity.product.*;
import pl.echoprint.digitalprintproject.service.*;

import java.math.BigDecimal;

/**
 * Dodaje inicjalne dane do bazy.
 */
@Component
@RequiredArgsConstructor
public class DataInitializer implements ApplicationListener<ContextRefreshedEvent> {

    private final PricingService pricingService;
    private final PaperService paperService;

    @Override
    public void onApplicationEvent(final ContextRefreshedEvent contextRefreshedEvent) {

        addPriceToCategory(ProductCategory.WIZYTOWKA, 50, BigDecimal.valueOf(45));
        addPriceToCategory(ProductCategory.WIZYTOWKA, 100, BigDecimal.valueOf(48));
        addPriceToCategory(ProductCategory.WIZYTOWKA, 250, BigDecimal.valueOf(49));
        addPriceToCategory(ProductCategory.WIZYTOWKA, 1000, BigDecimal.valueOf(92));

        addPriceToCategory(ProductCategory.ULOTKA, 500, BigDecimal.valueOf(94));
        addPriceToCategory(ProductCategory.ULOTKA, 1000, BigDecimal.valueOf(100));

        addPaperToCategory(ProductCategory.WIZYTOWKA, PaperCodeName.KREDA, 350, PaperFinishType.MATTE, "330x480");
        addPaperToCategory(ProductCategory.WIZYTOWKA, PaperCodeName.KREDA, 350, PaperFinishType.GLOSS, "330x480");
        addPaperToCategory(ProductCategory.ULOTKA, PaperCodeName.KREDA, 130, PaperFinishType.MATTE, "A6-105x148");
        addPaperToCategory(ProductCategory.ULOTKA, PaperCodeName.OLLIN, 110, PaperFinishType.GLOSS, "A5-148x210");
    }

    private void addPriceToCategory(final ProductCategory category, final int volume, final BigDecimal netPrice) {
        if (!pricingService.existsPricing(category, volume)) {
            pricingService.addPriceToCategory(new Price(volume, netPrice, category));
        }
    }

    private void addPaperToCategory(final ProductCategory category, final PaperCodeName paperCodeName,
                                    final Integer weight, final PaperFinishType paperFinishType,
                                    final String format) {
        if (!paperService.existsPaper(category, weight)) {
            paperService.createNewPaper(new Paper(paperCodeName, weight, paperFinishType, format, category));
        }
    }

}
