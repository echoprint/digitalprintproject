package pl.echoprint.digitalprintproject.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.echoprint.digitalprintproject.entity.cart.Cart;
import pl.echoprint.digitalprintproject.entity.product.Product;
import pl.echoprint.digitalprintproject.repository.CartRepository;

import java.math.BigDecimal;
import java.util.List;


@Service
@RequiredArgsConstructor
public class CartService {

    private final CartRepository cartRepository;
    private final UserContextService userContextService;

    public Cart getCurrentUserCart() {
        return userContextService.getCurrentCustomer().getCart();
    }

    public void addProductToCart(Product product) {
        Cart cart = getCurrentUserCart();
        List<Product> products = cart.getProductsInCart();
        products.add(product);

        cartRepository.save(cart);
    }

    public void removeProductFromCart(Product product) {
        Cart cart = getCurrentUserCart();
        List<Product> products = cart.getProductsInCart();
        products.remove(product);

        cartRepository.save(cart);
    }

    public void clearCart() {
        getCurrentUserCart().getProductsInCart().clear();

    }

    public BigDecimal calculateTotalCost() {

        Cart cart = getCurrentUserCart();
        return cart.getProductsInCart().stream()
                .map(pp -> pp.getPrice().getNetPrice().multiply(BigDecimal.valueOf(1.23)))
                .reduce(BigDecimal::add)
                .orElse(BigDecimal.ZERO);
    }
}
