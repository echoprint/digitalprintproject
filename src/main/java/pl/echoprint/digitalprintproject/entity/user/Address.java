package pl.echoprint.digitalprintproject.entity.user;

import lombok.*;
import pl.echoprint.digitalprintproject.entity.BaseEntity;

import javax.persistence.*;


@Getter
@Setter
@Builder
@Embeddable
@NoArgsConstructor
@AllArgsConstructor
public class Address {

    private String country;
    private String city;
    private String postalCode;
    private String street;
    private String buildingNumber;
    private String houseNumber;

}
