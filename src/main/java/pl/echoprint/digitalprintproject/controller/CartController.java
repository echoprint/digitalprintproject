package pl.echoprint.digitalprintproject.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import pl.echoprint.digitalprintproject.repository.ProductRepository;
import pl.echoprint.digitalprintproject.service.CartService;

import javax.servlet.http.HttpServletRequest;


@Controller
@RequiredArgsConstructor
public class CartController {

    private final ProductRepository productRepository;
    private final CartService cartService;

    @GetMapping("/add-to-cart/{productId}")
    public String addToCart(HttpServletRequest request,
                            @PathVariable Long productId) {
        productRepository.findProductById(productId).ifPresent(cartService::addProductToCart);

        return "redirect:" + request.getHeader("referer");
    }

    @GetMapping(value = "/products-in-cart")
    public String viewCart(Model model) {
        model.addAttribute("cartProductsList", cartService.getCurrentUserCart().getProductsInCart());
        model.addAttribute("totalPrice", cartService.calculateTotalCost().toString());

        return "displayCart";
    }

    @GetMapping(value = "/remove-product-from-cart/{prodId}")
    public String removeFromCart(@PathVariable Long prodId) {
        productRepository.findProductById(prodId).ifPresent(cartService::removeProductFromCart);

        return "redirect:/products-in-cart";
    }

}
