package pl.echoprint.digitalprintproject.service;


import org.springframework.security.crypto.password.PasswordEncoder;
import pl.echoprint.digitalprintproject.dto.CustomerRegistrationDto;
import pl.echoprint.digitalprintproject.entity.user.Address;
import pl.echoprint.digitalprintproject.entity.user.Customer;

public class UserRegistrationDtoToUserBuilder {

    public static Customer writeToCustomer(CustomerRegistrationDto dto, PasswordEncoder passwordEncoder) {
        Customer customer = new Customer();
        customer.setFirstName(dto.getFirstName().trim());
        customer.setLastName(dto.getLastName().trim());
        customer.setLogin(dto.getEmail().trim());
        customer.setHashedpw(passwordEncoder.encode(dto.getPassword().trim()));
        customer.setAddress(Address.builder()
                .city(dto.getCity().trim())
                .houseNumber(dto.getHouseNumber().trim())
                .buildingNumber(dto.getBuildingNumber().trim())
                .street(dto.getStreet().trim())
                .postalCode(dto.getPostalCode().trim())
                .country(dto.getCountry().trim())
                .build()
        );

        return customer;
    }
}
