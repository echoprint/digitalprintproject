package pl.echoprint.digitalprintproject.dto;

import lombok.Data;
import pl.echoprint.digitalprintproject.entity.product.Product;
import pl.echoprint.digitalprintproject.entity.product.ProductCategory;

import java.math.BigDecimal;
import java.util.Set;

@Data
public class PriceDto {

    public double volume;
    public BigDecimal netPrice;
    private ProductCategory productCategory;
    Set<Product> productsOrderedWithThisPrice;

}
