package pl.echoprint.digitalprintproject.service;

import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.SessionScope;
import pl.echoprint.digitalprintproject.entity.user.Customer;
import pl.echoprint.digitalprintproject.entity.user.RoleType;
import pl.echoprint.digitalprintproject.repository.CustomerRepository;


@Service
@SessionScope
@RequiredArgsConstructor
public class UserContextService {

    private final CustomerRepository customerRepository;

    public String getLoggedUserEmail() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication instanceof AnonymousAuthenticationToken) {
            return null;
        }
        return authentication.getName();
    }

    public boolean admin() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return authentication.getAuthorities().stream().anyMatch(e -> RoleType.ADMIN.getRoleName().equalsIgnoreCase(e.getAuthority()));
    }

    public boolean user() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return authentication.getAuthorities().stream().anyMatch(e -> RoleType.USER.getRoleName().equalsIgnoreCase(e.getAuthority()));
    }

    public boolean customer() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return authentication.getAuthorities().stream().anyMatch(e -> RoleType.CUSTOMER.getRoleName().equalsIgnoreCase(e.getAuthority()));
    }

    public Customer getCurrentCustomer() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication.getAuthorities().stream().anyMatch(e -> RoleType.CUSTOMER.getRoleName().equalsIgnoreCase(e.getAuthority()))) {
            System.out.println();
        }
        return customerRepository.findCustomerByLogin(getLoggedUserEmail()).get();
    }
}
