package pl.echoprint.digitalprintproject.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import pl.echoprint.digitalprintproject.dto.CustomerRegistrationDto;
import pl.echoprint.digitalprintproject.exception.UserExistsException;
import pl.echoprint.digitalprintproject.service.UserRegistrationService;

import javax.validation.Valid;

@RequiredArgsConstructor
@Controller
public class MainController {

    private final UserRegistrationService userRegistrationService;

    @GetMapping("/")
    public String home() {
        return "index";
    }

    @GetMapping("/login")
    public String login() {
        return "login";
    }

    @GetMapping(value = "/register")
    public String registerForm(Model model) {
        model.addAttribute("customerFormData", new CustomerRegistrationDto());
        return "registerForm";
    }

    @PostMapping(value = "/register")
    public String registerEffect(@ModelAttribute(name = "customerFormData")
                                 @Valid CustomerRegistrationDto customerFormData, Model model) {
        try {
            userRegistrationService.registerCustomer(customerFormData);
        } catch (UserExistsException e) {
            model.addAttribute("userExistsException", e.getMessage());
            return "registerForm";
        }
        model.addAttribute("registrationData", customerFormData);
        return "registerEffect";
    }
}
