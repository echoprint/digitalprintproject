package pl.echoprint.digitalprintproject.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import pl.echoprint.digitalprintproject.dto.PaperDto;
import pl.echoprint.digitalprintproject.entity.product.Paper;
import pl.echoprint.digitalprintproject.entity.product.PaperCodeName;
import pl.echoprint.digitalprintproject.entity.product.PaperFinishType;
import pl.echoprint.digitalprintproject.entity.product.ProductCategory;
import pl.echoprint.digitalprintproject.service.PaperService;

import java.util.List;

@Controller
@RequiredArgsConstructor
@RequestMapping(path = "/admin")
public class PaperController {

    private final PaperService paperService;

    @GetMapping("/add-paper")
    public String addPaper(Model model, @RequestParam (value= "category", required = false)
            ProductCategory productCategory, PaperDto paperDto) {
        if(productCategory != null){
            paperDto.setProductCategory(productCategory);
        }
        model.addAttribute("productCategoryType", ProductCategory.values());
        model.addAttribute("paperCodeName", PaperCodeName.values());
        model.addAttribute("paperFinishType", PaperFinishType.values());
        model.addAttribute("newPaper", new Paper());
        return "addPaper";
    }

    @PostMapping("/add-paper")
    public String addPaper(@ModelAttribute Paper paper) {
        paperService.createNewPaper(paper);

        return "redirect: /paper/paper-list";
    }


    @GetMapping("/paper-list")
    public String getPaperList(Model model) {
        List<Paper> paperList = paperService.find();
        model.addAttribute("paperList", paperList);
        return "paperList";
    }


}
