package pl.echoprint.digitalprintproject.entity.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.echoprint.digitalprintproject.entity.BaseEntity;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "users")
@Getter
@Setter
@NoArgsConstructor
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "userType", discriminatorType = DiscriminatorType.STRING)
public class User extends BaseEntity {

    @Column(nullable = false)
    private String login;

    @Column(nullable = false)
    private String hashedpw;

    private String firstName;
    private String lastName;

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<Role> role;

    private Address address;
}
