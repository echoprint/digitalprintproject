package pl.echoprint.digitalprintproject.service;

import org.springframework.stereotype.Service;
import pl.echoprint.digitalprintproject.dto.PaperDto;
import pl.echoprint.digitalprintproject.entity.product.Paper;
import pl.echoprint.digitalprintproject.repository.PaperRepository;

@Service
public class PaperToPaperDtoBuilder {

    private PaperRepository paperRepository;

    public PaperDto buildPaperDto(Paper paper){
        return PaperDto.builder()
                .id(paper.getId())
                .paperCodeName(paper.getPaperCodeName())
                .weight(paper.getWeight())
                .paperFinishType(paper.getPaperFinishType())
                .format(paper.getFormat())
                .productCategory(paper.getProductCategory())
                .build();
    }

    public Paper buildPaperEntity(PaperDto paperDto){
        Paper paper;
        if(paperDto.getId() == null){
            paper = new Paper();
        } else {
            paper = paperRepository.getOne(paperDto.getId());
        }
        paper.setPaperCodeName(paperDto.getPaperCodeName());
        paper.setWeight(paperDto.getWeight());
        paper.setPaperFinishType(paperDto.getPaperFinishType());
        paper.setFormat(paperDto.getFormat());
        paper.setProductCategory(paperDto.getProductCategory());

        return paper;

    }
}
