package pl.echoprint.digitalprintproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.echoprint.digitalprintproject.entity.product.Paper;
import pl.echoprint.digitalprintproject.entity.product.Product;
import pl.echoprint.digitalprintproject.entity.product.ProductCategory;
import pl.echoprint.digitalprintproject.entity.user.Customer;

import java.util.List;
import java.util.Optional;


public interface ProductRepository extends JpaRepository<Product, Long> {
    List<Product> findAll();

    Optional<Product> findProductById(final Long id);

    Optional<Product> findByPaper(Paper paper);

    List<Product> findProductsByProductCategory(ProductCategory productCategory);

}
