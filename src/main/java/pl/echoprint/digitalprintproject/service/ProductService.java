package pl.echoprint.digitalprintproject.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.echoprint.digitalprintproject.dto.ProductDto;
import pl.echoprint.digitalprintproject.entity.product.Product;
import pl.echoprint.digitalprintproject.entity.product.ProductCategory;
import pl.echoprint.digitalprintproject.repository.ProductRepository;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ProductService {

    private final ProductRepository productRepository;
    private final PaperService paperService;
    private final ProductToProductDtoBuilder productToProductDTOBuilder;

    public List<Product> find() {
        return productRepository.findAll();
    }

    private List<Product> findProductsByCategory(ProductCategory productCategory) {
        return productRepository.findProductsByProductCategory(productCategory);
    }

    public Optional<ProductDto> findProductById(Long id) {
        return productRepository.findProductById(id).map(productToProductDTOBuilder::buildDto);
    }

    public void createNewProduct(final ProductDto dto) {
        Product product = new Product();
        product.setPaper(paperService.getPaperById(dto.getPaperId()));
        product.setEndProducFormat(dto.getEndProducFormat());
        product.setProductCategory(dto.getProductCategory());
        product.setPrice(dto.getPrice());
        product.setNumberOfCopies(dto.getNumberOfCopies());
        product.setColors(dto.getColors());
        product.setDuplex(dto.isDuplex());

        productRepository.save(product);
    }

    public void updateProduct(ProductDto productDto) {
        Product s = productToProductDTOBuilder.buildEntity(productDto);
        productRepository.save(s);
    }
}