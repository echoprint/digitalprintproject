package pl.echoprint.digitalprintproject.dto;

import lombok.Data;
import pl.echoprint.digitalprintproject.entity.user.User;

import javax.validation.constraints.NotNull;

@Data
public class AddressDto {

    private Long id;
    private String country;
    private String city;
    private String postalCode;
    private String street;
    private String buildingNumber;
    private String houseNumber;
    @NotNull
    private User user;

}
