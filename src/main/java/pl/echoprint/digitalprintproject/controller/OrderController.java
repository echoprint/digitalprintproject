package pl.echoprint.digitalprintproject.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.echoprint.digitalprintproject.service.OrderService;

@Controller
@RequiredArgsConstructor
@RequestMapping(path = "/order")
public class OrderController {

    private final OrderService orderService;

    @RequestMapping(value= "/place-order")
    public String makeAnOrder (Model model){
        model.addAttribute("order", orderService.placeOrder());
        return "orderFinished";
    }
}
