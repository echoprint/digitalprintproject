package pl.echoprint.digitalprintproject.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.echoprint.digitalprintproject.entity.product.Price;
import pl.echoprint.digitalprintproject.entity.product.ProductCategory;
import pl.echoprint.digitalprintproject.repository.PriceRepository;

import java.util.List;


@Service
@RequiredArgsConstructor
public class PricingService {
    private final PriceRepository priceRepository;


    public void addPriceToCategory(final Price price) {
        priceRepository.save(price);
    }

    public boolean existsPricing(final ProductCategory category, final int volume) {
        return priceRepository.existsByProductCategoryAndVolume(category, volume);
    }

    public List<Price> getAll() {
        return priceRepository.findAll();
    }

    public List<Price> getPriceByCategory(final ProductCategory productCategory) {
        return priceRepository.findAllByProductCategory(productCategory);
    }
}
