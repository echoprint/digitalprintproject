package pl.echoprint.digitalprintproject.service;

import com.google.common.collect.Sets;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import pl.echoprint.digitalprintproject.dto.CustomerRegistrationDto;
import pl.echoprint.digitalprintproject.entity.cart.Cart;
import pl.echoprint.digitalprintproject.entity.user.Customer;
import pl.echoprint.digitalprintproject.entity.user.Role;
import pl.echoprint.digitalprintproject.entity.user.RoleType;
import pl.echoprint.digitalprintproject.exception.UserExistsException;
import pl.echoprint.digitalprintproject.repository.CartRepository;
import pl.echoprint.digitalprintproject.repository.CustomerRepository;
import pl.echoprint.digitalprintproject.repository.RoleRepository;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserRegistrationService {

    private final PasswordEncoder passwordEncoder;
    private final RoleRepository roleRepository;
    private final CustomerRepository customerRepository;
    private final CartRepository cartRepository;

    public void registerCustomer(CustomerRegistrationDto customerRegistrationDto) {
        Customer customer = UserRegistrationDtoToUserBuilder.writeToCustomer(customerRegistrationDto, passwordEncoder);
        if (customerRepository.existsCustomerByLogin(customer.getLogin())) {
            throw new UserExistsException("Customer with username " + customer.getLogin() + "already exists in database");
        } else {
            addCustomer(customer);
        }
    }

    private void addCustomer(Customer customer) {
        Role roleCustomer = Optional.ofNullable(roleRepository.findRoleByRoleName("ROLE_CUSTOMER"))
                .orElseGet(() -> roleRepository.save(new Role(RoleType.CUSTOMER.getRoleName())));
        customer.setCart(new Cart());
        cartRepository.save(customer.getCart());
        customer.getCart().setOwner(customer);

        customer.setRole(Sets.newHashSet(roleCustomer));
        customerRepository.save(customer);
        cartRepository.save(customer.getCart());
    }
}
