package pl.echoprint.digitalprintproject.service;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import pl.echoprint.digitalprintproject.entity.user.User;
import pl.echoprint.digitalprintproject.repository.CustomerRepository;
import pl.echoprint.digitalprintproject.repository.UserRepository;

import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class LoginService implements UserDetailsService {
    private final UserRepository userRepository;
    private final CustomerRepository customerRepository;

    @Override
    public UserDetails loadUserByUsername(final String s) throws UsernameNotFoundException {
        User u = userRepository
                .findUserByLogin(s)
                .orElse(customerRepository
                        .findCustomerByLogin(s)
                        .orElseThrow(() -> new UsernameNotFoundException(s + " not found")));

        return org.springframework.security.core.userdetails.User.builder()
                .username(u.getLogin())
                .password(u.getHashedpw())
                .roles(u.getRole()
                        .stream()
                        .map(role -> role.getRoleName().replace("ROLE_", ""))
                        .collect(Collectors.toList())
                        .toArray(new String[u.getRole().size()]))
                .disabled(false)
                .build();
    }
}
