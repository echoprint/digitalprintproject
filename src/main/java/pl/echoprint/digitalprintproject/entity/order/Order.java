package pl.echoprint.digitalprintproject.entity.order;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;
import pl.echoprint.digitalprintproject.entity.BaseEntity;
import pl.echoprint.digitalprintproject.entity.cart.Cart;
import pl.echoprint.digitalprintproject.entity.user.Address;
import pl.echoprint.digitalprintproject.entity.user.Customer;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Table(name = "orders")
@ToString(exclude = "customer")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Order extends BaseEntity implements Serializable {

    private BigDecimal totalCost;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "country", column = @Column(name = "customer_country")),
            @AttributeOverride(name = "city", column = @Column(name = "customer_city")),
            @AttributeOverride(name = "postalCode", column = @Column(name = "customer_postal_code")),
            @AttributeOverride(name = "street", column = @Column(name = "customer_street")),
            @AttributeOverride(name = "buildingNumber", column = @Column(name = "customer_buildingNumber")),
            @AttributeOverride(name = "houseNumber", column = @Column(name = "customer_houseNumber"))})
    private Address customerAddress;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "country", column = @Column(name = "delivery_country")),
            @AttributeOverride(name = "city", column = @Column(name = "delivery_city")),
            @AttributeOverride(name = "postalCode", column = @Column(name = "delivery_postal_code")),
            @AttributeOverride(name = "street", column = @Column(name = "delivery_street")),
            @AttributeOverride(name = "buildingNumber", column = @Column(name = "delivery_buildingNumber")),
            @AttributeOverride(name = "houseNumber", column = @Column(name = "delivery_houseNumber"))})
    private Address deliveryAddress;


    @JsonFormat(pattern = "dd/MM/yyyy@HH:mm:ss.SSSZ")
    private LocalDateTime dateTimeCreated;

    @Enumerated(EnumType.STRING)
    @Column(name = "order_status", nullable = false)
    private OrderStatus status;

    @ManyToOne
    @JoinColumn(name = "id_customer")
    private Customer customer;

    @OneToOne
    private Cart cart;

}
