package pl.echoprint.digitalprintproject.entity.product;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.echoprint.digitalprintproject.entity.BaseEntity;

import javax.persistence.*;


@Entity
@Table(name = "product")
@Getter
@Setter
@NoArgsConstructor
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class Product extends BaseEntity {

    @Enumerated(EnumType.STRING)
    @Column(name = "product_type", nullable = false)
    private ProductCategory productCategory;

    @ManyToOne
    private Paper paper;

    private String colors;

    private String endProducFormat;

    boolean duplex;

    private Integer numberOfCopies;

    @ManyToOne
    private Price price;



}
