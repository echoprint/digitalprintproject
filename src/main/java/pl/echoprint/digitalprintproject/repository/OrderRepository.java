package pl.echoprint.digitalprintproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.echoprint.digitalprintproject.entity.order.Order;

import java.util.Optional;

public interface OrderRepository extends JpaRepository<Order, Long> {

    Optional<Order> findById(Long id);

}
