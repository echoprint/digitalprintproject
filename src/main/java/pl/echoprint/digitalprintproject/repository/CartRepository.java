package pl.echoprint.digitalprintproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.echoprint.digitalprintproject.entity.cart.Cart;
import pl.echoprint.digitalprintproject.entity.user.Customer;

import java.util.Optional;

public interface CartRepository extends JpaRepository<Cart, Long> {

    Cart getCartByOwner(Customer owner);

    Optional<Cart> findCartById(Long cartId);



}
