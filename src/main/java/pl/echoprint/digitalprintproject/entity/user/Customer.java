package pl.echoprint.digitalprintproject.entity.user;

import com.google.common.collect.Lists;
import lombok.Getter;
import lombok.Setter;
import pl.echoprint.digitalprintproject.entity.cart.Cart;
import pl.echoprint.digitalprintproject.entity.order.Order;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@DiscriminatorValue("customer")
public class Customer extends User {

    @OneToMany(mappedBy = "customer", cascade = CascadeType.ALL)
    private List<Order> orderList = Lists.newArrayList();

    @OneToOne(mappedBy = "owner")
    private Cart cart;

}
