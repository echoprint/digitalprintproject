package pl.echoprint.digitalprintproject.service;

import org.springframework.stereotype.Service;
import pl.echoprint.digitalprintproject.dto.ProductDto;
import pl.echoprint.digitalprintproject.entity.BaseEntity;
import pl.echoprint.digitalprintproject.entity.product.Product;
import pl.echoprint.digitalprintproject.repository.PaperRepository;
import pl.echoprint.digitalprintproject.repository.ProductRepository;

import java.util.Optional;

@Service
public class ProductToProductDtoBuilder {

    private PaperRepository paperRepository;

    private ProductRepository productRepository;

    public ProductDto buildDto(Product product) {
        return ProductDto.builder()
                .id(product.getId())
                .productCategory(product.getProductCategory())
                .endProducFormat(product.getEndProducFormat())
                .paperId(Optional.ofNullable(product.getPaper()).map(BaseEntity::getId).orElse(null))
                .colors(product.getColors())
                .duplex(product.isDuplex())
                .numberOfCopies(product.getNumberOfCopies())
                .price(product.getPrice())
                .build();
    }

    public Product buildEntity(ProductDto dto) {
        Product product;
        if (dto.getId() == null) {
            product = new Product();
        } else {
            product = productRepository.getOne(dto.getId());
        }
        product.setProductCategory(dto.getProductCategory());
        product.setPaper(Optional.ofNullable(dto.getPaperId()).map(paperRepository::getOne).orElse(null));
        product.setColors(dto.getColors());
        product.setDuplex(dto.isDuplex());
        product.setNumberOfCopies(dto.getNumberOfCopies());
        product.setPrice(dto.getPrice());

        return product;
    }
}
