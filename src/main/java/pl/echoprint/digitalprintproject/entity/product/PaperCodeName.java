package pl.echoprint.digitalprintproject.entity.product;

public enum PaperCodeName {
    KREDA, OFFSET, OLLIN
}
