package pl.echoprint.digitalprintproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.echoprint.digitalprintproject.entity.user.Customer;
import pl.echoprint.digitalprintproject.entity.user.User;

import java.util.List;
import java.util.Optional;

public interface CustomerRepository extends JpaRepository<Customer, Long> {

    List<Customer> findAll();

    Optional<Customer> findCustomerByLogin(final String login);

    boolean existsCustomerByLogin(String login);
}
