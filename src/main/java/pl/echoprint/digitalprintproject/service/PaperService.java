package pl.echoprint.digitalprintproject.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.echoprint.digitalprintproject.dto.PaperDto;
import pl.echoprint.digitalprintproject.entity.product.Paper;
import pl.echoprint.digitalprintproject.entity.product.PaperCodeName;
import pl.echoprint.digitalprintproject.entity.product.Product;
import pl.echoprint.digitalprintproject.entity.product.ProductCategory;
import pl.echoprint.digitalprintproject.repository.PaperRepository;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class PaperService {

    private final PaperRepository paperRepository;

    private PaperToPaperDtoBuilder paperToPaperDtoBuilder;

    public List<Paper> find() {
        return paperRepository.findAll();
    }

    public Optional<Paper> findPaperById(Long id) {
        return paperRepository.findById(id);
    }

    public Optional<Paper> findPaperByCodeName(PaperCodeName paperCodeName) {
        return paperRepository.findByPaperCodeName(paperCodeName);
    }

    @Transactional
    public void updatePaper(PaperDto paperDto) {
        Paper paper = paperToPaperDtoBuilder.buildPaperEntity(paperDto);
        paperRepository.save(paper);
    }

    public void deletePaper(Long id) {
        paperRepository.deleteById(id);
    }

    public Paper getPaperById(final Long paperParam) {
        return paperRepository.getOne(paperParam);
    }


    public void createNewPaper(final Paper paper) {
        paperRepository.save(paper);
    }

    public List<Paper> getPaperByProductCategory(final ProductCategory productCategory) {
        return paperRepository.findAllByProductCategory(productCategory);
    }

    public boolean existsPaper(final ProductCategory productCategory, final int weight) {
        return paperRepository.existsByProductCategoryAndAndWeight(productCategory, weight);
    }

    public List<Paper> getAll() {
        return paperRepository.findAll();
    }
}
