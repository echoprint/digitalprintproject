package pl.echoprint.digitalprintproject.entity.user;

import lombok.Getter;

@Getter
public enum RoleType {
    ADMIN("ROLE_ADMIN"), USER("ROLE_USER"),
    CUSTOMER("ROLE_CUSTOMER");

    private String roleName;

    RoleType(final String roleName) {
        this.roleName = roleName;
    }
}
